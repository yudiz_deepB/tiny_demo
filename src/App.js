import React from 'react';
import TinyMceEditor from './Component/TinyMceEditor';

const App = () => {
  return (
    <div>
      <h1>
        <center>Tiny Demo</center>
      </h1>
      <TinyMceEditor />
    </div>
  );
};

export default App;
